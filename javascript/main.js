const $ = document;
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves";

//Receber lobos e adcionar em Exemplos
fetch(url).then((resp) => resp.json()).then((resp) => {
    //Escolher lobo aleatorio
    let primeiroLobo = resp[Math.floor(Math.random() * resp.length)]
    let segundoLobo = resp[Math.floor(Math.random() * resp.length)]

    //Nao pegar o mesmo
    while (primeiroLobo.name === segundoLobo.name){
        segundoLobo = resp[Math.floor(Math.random() * resp.length)]
    }
    console.log(resp)
    console.log(primeiroLobo)
    console.log(segundoLobo)

    //Pegando atributos
    let primeiroNome = primeiroLobo.name;
    let primeiroIdade = primeiroLobo.age;
    let primeiroTexto = primeiroLobo.description;
    let primeiroImagem = primeiroLobo.image;

    //Resolvendo null
    if (primeiroIdade === null){
        primeiroIdade = "Desconhecida"
    }

    if (primeiroTexto === null || primeiroTexto === ""){
        primeiroTexto = "Não há descrição"
    }


    let segundoNome = segundoLobo.name;
    let segundoIdade = segundoLobo.age;
    let segundoTexto = segundoLobo.description;
    let segundoImagem = segundoLobo.image;

    //Resolvendo null
    if (segundoIdade === null){
        segundoIdade = "Desconhecida"
    }

    if (segundoTexto === null || segundoTexto === ""){
        segundoTexto = "Não há descrição"
    }

    //Atribuindo a elementos html
    let nomeElemento1 = $.querySelector("#nomeExemplo1");
    nomeElemento1.innerText = primeiroNome;

    let idadeElemento1 = $.querySelector("#idadeExemplo1");
    idadeElemento1.innerText = `Idade: ${primeiroIdade}`;

    let textoElemento1 = $.querySelector("#textoExemplo1");
    textoElemento1.innerText = primeiroTexto;
    
    let imagemElemento1 = $.querySelector("#imagem1");
    imagemElemento1.src = primeiroImagem;

    //segundo exemplo

    let nomeElemento2 = $.querySelector("#nomeExemplo2");
    nomeElemento2.innerText = segundoNome;

    let idadeElemento2 = $.querySelector("#idadeExemplo2");
    idadeElemento2.innerText = `Idade: ${segundoIdade}`;

    let textoElemento2 = $.querySelector("#textoExemplo2");
    textoElemento2.innerText = segundoTexto;
    
    let imagemElemento2 = $.querySelector("#imagem2");
    imagemElemento2.src = segundoImagem;

})

