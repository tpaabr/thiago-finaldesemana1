const $ = document;
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves";
const botaoAdotar = $.querySelector("#butAdotar");


const myHeaders = {
    "content-Type":"application/json"
}

//Recebendo qual id Especifico 
const paramUrl = window.location.search;
let idLobo = parseInt(paramUrl.slice(4))

const urlDelete = url + `/${idLobo}`

fetch(url).then((resp) => resp.json()).then((resp) => {

    //pegando o objeto Certo
    for (let i = 0; i < resp.length; i++) {
        if (resp[i].id === idLobo) {

            let nomeShow = resp[i].name;
            let imagemShow = resp[i].image;
            let idadeShow = resp[i].age;
            let descShow = resp[i].description;

            let nome = $.querySelector("#nomeShow");
            nome.innerText = nomeShow;

            let imagem = $.querySelector("#showFoto")
            imagem.src = imagemShow;

            let novoId = $.querySelector("#idLobo");
            novoId.innerText = `ID: ${idLobo}`

        }
      
    }

})


const fetchConfigDel = {
    method: "DELETE",
    headers: myHeaders
}



//Mandando Lobo mudado
botaoAdotar.addEventListener("click", () => {

    fetch(url).then((resp) => resp.json()).then((resp) => {
        console.log("ate aqui foi")

        for (let i = 0; i < resp.length; i++) {
            if (resp[i].id === idLobo) {

                //Primeiro Deletando para adcionar Lobo igual ao anterior com atributos mudados
                fetch(urlDelete, fetchConfigDel).then(console.log);
    
                let nomeShow = resp[i].name;
                let imagemShow = resp[i].image;
                let idadeShow = resp[i].age;
                let descShow = resp[i].description;
    
                let nomeGuard = $.querySelector("#nomeAdotar").value;
                let idadeGuard = $.querySelector("#idadeAdotar").value;
                let emailGuard = $.querySelector("#emailAdotar").value;
    
                //Enviando Adoção
            
                var novoPost = {
                     "wolf": {
    
                        "name": nomeShow,
                        "id": idLobo,
                        "age": idadeShow,
                        "description": descShow,
                        "adopted": true,
                        "guardian_name": nomeGuard,
                        "guardian_age":idadeGuard,
                        "guardian_emails":emailGuard,
                        "image": imagemShow
                                    
                    }
                }
                            
                var fetchConfig = {
                     method: "POST",
                    headers: myHeaders,
                    body: JSON.stringify(novoPost)
                }
    
                fetch(url, fetchConfig).then(console.log).then(alert("Adoção registrada"));
            }
          
        }
    })
})

