const $ = document;
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves";

//Recebendo qual id Especifico 
const paramUrl = window.location.search;
let idLobo = parseInt(paramUrl.slice(4))


const urlDelete = url + `/${idLobo}`

console.log(urlDelete)

const myHeaders = {
    "content-Type":"application/json"
}

const fetchConfig = {
    method: "DELETE",
    headers: myHeaders

}

const botaoAdotar = $.querySelector("#adotar");
botaoAdotar.href = `adotarLobinhos.html?id=${idLobo}`;

//botao Remover
const botaoRemove = $.querySelector("#excluir");



botaoRemove.addEventListener("click", () => {
    fetch(urlDelete, fetchConfig).then(console.log).then(alert("Lobo ELIMINADO"))
    
})


console.log(idLobo)

fetch(url).then((resp) => resp.json()).then((resp) => {

    console.log(resp)

    //pegando o objeto Certo
    for (let i = 0; i < resp.length; i++) {
        if (resp[i].id === idLobo) {

            let nomeShow = resp[i].name;
            let idadeShow = resp[i].age;
            let descShow = resp[i].description;
            let imagemShow = resp[i].image;

            let nome = $.querySelector("#nomeShow");
            nome.innerText = nomeShow;

            let idade = $.querySelector("#idadeShow");
            idade.innerText = `Idade: ${idadeShow}`;

            let desc = $.querySelector("#textoShow");
            desc.innerText = descShow;


            let imagem = $.querySelector("#showFoto")
            imagem.src = imagemShow;

        }
        
    }
})