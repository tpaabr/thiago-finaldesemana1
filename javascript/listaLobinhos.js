const $ = document;
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves";
const listaLobos = $.querySelector("#listaLobos")

//Receber lobos e adcionar em Exemplos
fetch(url).then((resp) => resp.json()).then((resp) => {

    for (let i = 0; i < resp.length; i++) {
        let currLobo = resp[i]

        //Pegando atributos
        let currId = currLobo.id;
        let currNome = currLobo.name;
        let currIdade = currLobo.age;
        let currTexto = currLobo.description;
        let currImagem = currLobo.image;

        let itemExemplo = $.createElement("div");
        itemExemplo.classList.add("itemExemplo");

        let infoExemplo = $.createElement("div");
        infoExemplo.classList.add("infoExemplo");

        //Resolvendo null
        if (currIdade === null){
            currIdade = "Desconhecida"
        }

        if (currTexto === null || currTexto === ""){
            currTexto = "Não há descrição"
        }

        //Alternar entre Esquerda e Direita
        if(i % 2 === 0) {

            let botaoAdotar = $.createElement("a");
            botaoAdotar.classList.add("linkAdotar")
            botaoAdotar.innerText = "Adotar";
            botaoAdotar.href = `showLobinho.html?id=${currId}`;

            let nomeElemento1 = $.createElement("h3");
            nomeElemento1.classList.add("nomeExemplo1")
            nomeElemento1.innerText = currNome;

            let idadeElemento1 = $.createElement("p");
            idadeElemento1.classList.add("idadeExemplo1")
            idadeElemento1.innerText = `Idade: ${currIdade}`;

            let textoElemento1 = $.createElement("p");
            textoElemento1.classList.add("textoExemplo1")
            textoElemento1.innerText = currTexto;

            infoExemplo.appendChild(botaoAdotar);
            infoExemplo.appendChild(nomeElemento1);
            infoExemplo.appendChild(idadeElemento1);
            infoExemplo.appendChild(textoElemento1);
            
            let container1 = $.createElement("div");
            container1.classList.add("container1")

            let imagemElemento1 = $.createElement("img");
            imagemElemento1.src = currImagem;

            container1.appendChild(imagemElemento1);

            itemExemplo.appendChild(container1);
            itemExemplo.appendChild(infoExemplo);

        } else {

            let botaoAdotar = $.createElement("a");
            botaoAdotar.classList.add("linkAdotar")
            botaoAdotar.innerText = "Adotar";
            botaoAdotar.href = `showLobinho.html?id=${currId}`;

            let nomeElemento2 = $.createElement("h3");
            nomeElemento2.classList.add("nomeExemplo2")
            nomeElemento2.innerText = currNome;

            let idadeElemento2 = $.createElement("p");
            idadeElemento2.classList.add("idadeExemplo2")
            idadeElemento2.innerText = `Idade: ${currIdade}`;

            let textoElemento2 = $.createElement("p");
            textoElemento2.classList.add("textoExemplo2")
            textoElemento2.innerText = currTexto;

            infoExemplo.appendChild(botaoAdotar);
            infoExemplo.appendChild(nomeElemento2);
            infoExemplo.appendChild(idadeElemento2);
            infoExemplo.appendChild(textoElemento2);
            
            let container2 = $.createElement("div");
            container2.classList.add("container2")

            let imagemElemento2 = $.createElement("img");
            imagemElemento2.src = currImagem;

            container2.appendChild(imagemElemento2);

            
            itemExemplo.appendChild(infoExemplo);
            itemExemplo.appendChild(container2);

        }

        //Adcionando ao mais geral
        listaLobos.appendChild(itemExemplo)
        
    }

})

