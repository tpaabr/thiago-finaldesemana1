const $ = document;
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves";
const sendBut = $.querySelector("#botaoEnviar");


const myHeaders = {
    "content-Type": "application/json"
}

sendBut.addEventListener("click", () => {
    let nomeLobo = $.querySelector("#nomeLobo").value;
    let idadeLobo = $.querySelector("#idadeLobo").value;
    let fotoLobo = $.querySelector("#fotoLobo").value;
    let descLobo = $.querySelector("#descLobo").value;

    idadeLobo = parseInt(idadeLobo);

    let novoPost = {
        "wolf": {
            "name": nomeLobo,
            "age": idadeLobo,
            "description": descLobo,
            "image": fotoLobo

        }
    }

    let fetchConfig = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(novoPost)
    }


    fetch(url, fetchConfig).then(console.log).then(alert("Lobo registrado com sucesso"))


})

